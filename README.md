# Создание образа для развертывания приложения АТОИР через GitlabCI

1. Создайте образ файла
```
docker build -t atiorimg:latest .
```
2. Смотрим IMAGE ID для образа atoirimg:latest
```
docker images
```
3. Конектимся к нашему регистри (сейчас написано для глобального докер регистри, нужно исправить комманду для локального)
```
docker login --username=USERNAME
```
4. Создаем тег для нашего образа
```
docker tag IMAGE_IG REGISTRY_NAME/atoirimg:latest
```
5. Пушим образ в регистри
```
docker push REGISTRY_NAME/atoirimg:latest
```

теперь наш образ для файла деплоя называется REGISTRY_NAME/atoirimg:latest его и используем при билде 