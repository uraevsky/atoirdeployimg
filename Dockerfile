FROM 	mhart/alpine-node:10.16.3
RUN 	mkdir -p /home/app
RUN 	addgroup -S app &&\
    	adduser -S app -G app -h /home/app -s /sbin/nologin &&\
    	addgroup -S www-data &&\
    	addgroup app www-data
ENV 	HOME=/home/app
ENV 	APP_HOME=/home/app/atoir-app
ENV 	APP_FILES=/home/app/files
ENV 	APP_FILES_SERVICE_DESK=/home/app/files/service_desc
ENV 	APP_FILES_ORGANIZATION_EQUIPMENTS=/home/app/files/organization_equipments
ENV 	APP_FILES_IMAGES=/home/app/files/images
ENV 	APP_FILES_USER_AVATARS=/home/app/files/images/user_avatars
ENV 	APP_FILES_REPORT_TEMPLATES=/home/app/files/report_templates
ENV 	APP_FILES_TEMP_ROOT=/home/app/files/temp
ENV 	APP_PM2_LOGS=/home/app/.pm2
ENV 	APP_LOGS=/home/app/logs
RUN 	mkdir $APP_HOME && mkdir $APP_FILES && mkdir $APP_PM2_LOGS && mkdir $APP_LOGS && mkdir $APP_FILES_IMAGES && mkdir $APP_FILES_USER_AVATARS && mkdir $APP_FILES_SERVICE_DESK && mkdir $APP_FILES_REPORT_TEMPLATES && mkdir $APP_FILES_TEMP_ROOT
RUN 	apk update \
    	&& apk --no-cache --virtual deps add \
      	python2 \
      	make \
    	&& npm add --global node-gyp